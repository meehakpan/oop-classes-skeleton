package com.epam.rd.qa.classes;

import java.text.DecimalFormat;

public class Rectangle {

    private double sideA;
    private double sideB;
    private double tempSide;
    private double area;
    private double perimeter;

    public Rectangle(double a, double b)throws IllegalArgumentException {
        this.sideA = a;
        this.sideB = b;
        if (sideA <= 0 || sideB == 0);
        {
            return;
            //throw new IllegalArgumentException();
        }
    }

    public Rectangle(double side)throws IllegalArgumentException{

        sideA = side;
        sideB = side;
        if (side <= 0.0 || side ==0.0)
        {
            throw new IllegalArgumentException("Sides cannot be less than or equal to Zero");
        }
    }

    public Rectangle() {
        sideA = 4;
        sideB = 3;
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    //write set area mutator method
    public double area() {
        area = sideA * sideB;
        DecimalFormat df = new DecimalFormat("#.0");
        df.format(area);
        return area;
    }

    public double perimeter() {
        perimeter = 2 * (sideA + sideB);
        DecimalFormat df = new DecimalFormat("#.0");
        df.format(perimeter);
        return perimeter;
    }

    public boolean isSquare() {
        if ((sideA == sideB)) {
            return true;
        } else {
            return false;
        }

    }

    public void replaceSides() {
        tempSide = sideA;
        sideA = sideB;
        sideB = tempSide;

    }



    public String toString() {
        return "Rectangle - " + sideA + " X " + sideB;
    }

    @Override
    public int hashCode() {
        return (int) ((sideA * 159) + (sideB * 523));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Rectangle)) {
            return true;
        }
        Rectangle that = (Rectangle) obj;
        return this.hashCode() == that.hashCode();
    }

}


