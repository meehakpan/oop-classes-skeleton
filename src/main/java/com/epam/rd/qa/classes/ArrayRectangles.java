package com.epam.rd.qa.classes;

import java.util.Arrays;

public class ArrayRectangles {

    private Rectangle[] rectangleArray;

    public ArrayRectangles(int size) {
        if (rectangleArray.length <= 0)
            throw new IllegalArgumentException();
        //this.rectangleArray = new Rectangle[size];
        Rectangle[] rectangleArray = new Rectangle[size];
        for (int i = 0; i <= size; ++i) {
            rectangleArray[i] = new Rectangle();
        }

    }

    public Rectangle[] getRectangleArray() {
        return rectangleArray;
    }

    public ArrayRectangles(Rectangle... rectangles) {
        if (rectangleArray == null || rectangleArray.length == 0) {
            throw new IllegalArgumentException();
        }

        //rectangleArray = rectangles;
        rectangles = new Rectangle[rectangles.length];
        for (int i = 0; rectangles.length > i; i++) {
            if (rectangles[i] != null) {
                rectangles[i] = new Rectangle(rectangles[i].getSideA(), rectangles[i].getSideB());
            }
        }
    }

    public boolean addRectangle(Rectangle rectangle) {
        {
            boolean isEmpty = false;
            //var openArray = 0;
            for (int i = 0; i < rectangleArray.length; i++) {
                //if (rectangleArray[i] == null) {
                if (rectangle != null) {
                    rectangleArray[i] = rectangle;
                    // empty = true;
                    //  break;
                    isEmpty = true;
                }
            }
            // return empty;
            return isEmpty;
        }
    }

    public int size() {
        // TODO place your code here
        throw new UnsupportedOperationException();
    }

    /**
     * boolean isEmpty;
     * isEmpty = ArrayRectangles.addRectangle();
     * <p>
     * if (isEmpty) {
     * } else {
     * <p>
     * <p>
     * Rectangle[] newRectArray = new Rectangle[0];
     * Rectangle[] length = newRectArray;
     * Rectangle[] rectArrValue = newRectArray;
     * <p>
     * // if(newRectArray.addRectangle()) {
     * //do something
     * }
     * return 0;
     * }
     */

    public int indexMaxArea() {
        if (rectangleArray == null || rectangleArray.length == 0)
            return -1; // null or empty

        int max_Area = Integer.MIN_VALUE;
        int index = -1;
        // loop to iterate through all rectangles
        // and keep track of max area
        for (int i = 0; i < rectangleArray.length; i++) {
            int temp_area = (int) (rectangleArray[i].getSideA() * rectangleArray[i].getSideB());
            if (temp_area > max_Area) {
                max_Area = temp_area;
            } else {
            }

        }
        return max_Area;
    }


    public int indexMinPerimeter () {
        double minPerimeter = rectangleArray[0].perimeter();
        int index = -1;
        for (int i = 0; i < rectangleArray.length; i++) {
            if (rectangleArray[i].perimeter() < minPerimeter) {
                minPerimeter = rectangleArray[i].perimeter();
                index = i;
            } else {
            }

        }
        return index;
    }

public int numberSquares () {
    return (int) Arrays.stream(rectangleArray).filter(Rectangle::isSquare).count();

}

    @Override
    public String toString() {
        return "ArrayRectangles{" +
                "rectangleArray=" + Arrays.toString(rectangleArray) +
                '}';
    }
}


