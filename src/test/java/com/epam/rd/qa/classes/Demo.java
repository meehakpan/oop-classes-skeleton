package com.epam.rd.qa.classes;

public class Demo {

    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(8, 5);
        Rectangle rectangle2 = new Rectangle(6, 4);

        Rectangle[] rectangles = new Rectangle[3];
        rectangles[0] = rectangle1;
        rectangles[1] = rectangle2;;
        rectangles[2] = new Rectangle(10, 10);;

        for(int i = 0; i < rectangles.length; i++){
            System.out.println(rectangle1);
        }

    }
}
